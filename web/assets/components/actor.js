define(["jquery", "select2"], function($) {
    function addOptionForm($collectionHolder, $newLinkLi) {
        let prototype = $collectionHolder.data('prototype');
        let index = $collectionHolder.data('index');
        let newForm = prototype;
        newForm = newForm.replace(/__name__/g, index);
        $collectionHolder.data('index', index + 1);
        let $newFormLi = $('<li></li>').append(newForm);
        $newLinkLi.before($newFormLi);
        addOptionFormDeleteLink($newFormLi);
        $("select").select2({tags:true});
    }


    function addOptionFormDeleteLink($optionLi) {
        let $removeButton = $('<button type="button">удалить</button>');
        $optionLi.append($removeButton);
        $removeButton.on('click', function(e) {
            $optionLi.remove();
        });
    }

    let $collectionHolderActors;
    let $addOptionButton = $('<button type="button" class="add_link">Добавить</button>');
    let $newLinkLi = $('<li></li>').append($addOptionButton);
    $(document).ready(function() {
        $collectionHolderActors = $('ul.actors');
        $collectionHolderActors.find('li').each(function() {
            addOptionFormDeleteLink($(this));
        });
        $collectionHolderActors.append($newLinkLi);
        $collectionHolderActors.data('index', $collectionHolderActors.find(':input').length);
        $addOptionButton.on('click', function(e) {
            addOptionForm($collectionHolderActors, $newLinkLi);
        });
    });
});
