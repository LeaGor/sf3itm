define(["jquery", "select2"], function($) {
    function addGenreForm($collectionHolder, $newLinkLi) {
        let prototype = $collectionHolder.data('prototype');
        let index = $collectionHolder.data('index');
        let newForm = prototype;
        newForm = newForm.replace(/__name__/g, index);
        $collectionHolder.data('index', index + 1);
        let $newFormLi = $('<li></li>').append(newForm);
        $newLinkLi.before($newFormLi);
        addGenreFormDeleteLink($newFormLi);
        $("select").select2({tags:true});
    }

    function addGenreFormDeleteLink($genreFormLi) {
        let $removeFormButton = $('<button type="button">удалить</button>');
        $genreFormLi.append($removeFormButton);
        $removeFormButton.on('click', function(e) {
            $genreFormLi.remove();
        });
    }

    let $collectionHolder;
    let $addGenreButton = $('<button type="button" class="add_genre_link">Добавить</button>');
    let $newLinkLi = $('<li></li>').append($addGenreButton);
    $(document).ready(function() {
        $collectionHolder = $('ul.genres');
        $collectionHolder.find('li').each(function() {
            addGenreFormDeleteLink($(this));
        });
        $collectionHolder.append($newLinkLi);
        $collectionHolder.data('index', $collectionHolder.find(':input').length);
        $addGenreButton.on('click', function(e) {
            addGenreForm($collectionHolder, $newLinkLi);
        });
    });
});
