define(["jquery", "select2"], function($) {
    $(document).ready(function() {
        $("select").select2({tags:true});
        $("select").prop("disabled", true);
        $("#movie_country select").prop("disabled", false);
        $("form").submit(function () {
            $("select").prop("disabled", false);
            return true;
        });
    })
})