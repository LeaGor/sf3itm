- git clone https://gitlab.com/LeaGor/sf3itm.git

- cd sf3itm

- composer install

- editing parameters.yml files

- DB:
```
php bin/console doctrine:database:create

php bin/console doctrine:schema:update --force
```
- composer install

- edit file web/assets/require.config.js => "baseUrl": "/assets"

- php bin/console server:start
