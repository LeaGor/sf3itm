<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Movie;
use AppBundle\Entity\Genre;
use AppBundle\Entity\Actor;
use AppBundle\Entity\Country;
use AppBundle\Form\MovieType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\Common\Collections\ArrayCollection;


class MoviesController extends Controller
{
    /**
     * @Route("/", name="movies_list")
     */
    public function indexAction()
    {
        $manager = $this->getDoctrine()->getManager();
        $movies = $manager->getRepository('AppBundle:Movie')->findAllDesc();
        return $this->render('movies/list.html.twig', [
            'movies' => $movies,
        ]);
    }

    /**
     * @Route("/add", name="movie_add")
     * @Method({"GET", "POST"})
     */
    public function addAction(Request $request)
    {
        $movie = new Movie();
        $form = $this->createForm(MovieType::class, $movie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            /*Genres*/
            $genres_repository = $entityManager->getRepository(Genre::class)->findAll();
            foreach ($genres_repository as $genre_repository) {
                foreach ($movie->getGenres() as $genre_request){
                    if($genre_request->getName() == $genre_repository->getName()){
                        $movie->getGenres()->removeElement($genre_request);
                        $movie->addGenre($genre_repository);
                    }
                }
            }

            /*Actors*/
            $actors_repository = $entityManager->getRepository(Actor::class)->findAll();
            foreach ($actors_repository as $actor_repository) {
                foreach ($movie->getActors() as $actors_request){
                    if($actors_request->getFullName() == $actor_repository->getFullName()){
                        $movie->getActors()->removeElement($actors_request);
                        $movie->addActor($actor_repository);
                    }
                }
            }

            /*Country*/
            $countries_repository = $entityManager->getRepository(Country::class)->findAll();
            //Если пытаемся добавить уже существующие в базе страну
            foreach ($countries_repository as $country_repository) {
                if($movie->getCountry()->getName() == $country_repository->getName()) {
                    $movie->setCountry($country_repository);
                }
            }

            /*Cover*/
            if($movie->getCover()){
                $file = $movie->getCover();
                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();
                $movie->setCover($fileName);
                $file->move('files', $fileName);
            }


            $entityManager->persist($movie);
            $entityManager->flush();
            return $this->redirectToRoute('movies_list');
        }
        return $this->render('movies/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="movie_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction($id, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $movie = $entityManager->getRepository(Movie::class)->find($id);

        $form = $this->createForm(MovieType::class, $movie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $genres_repository = $entityManager->getRepository(Genre::class)->findAll();
            $actors_repository = $entityManager->getRepository(Actor::class)->findAll();
            //$countries_repository = $entityManager->getRepository(Country::class)->findAll();

            foreach ($genres_repository as $genre_repository) {
                foreach ($movie->getGenres() as $genre_request){
                    if($genre_request->getName() == $genre_repository->getName()){
                        $movie->getGenres()->removeElement($genre_request);
                        $movie->addGenre($genre_repository);
                        $entityManager->persist($movie);
                    }
                    if ($movie->getGenres()->contains($genre_request) === false) {
                        $genre_request->getMovies()->removeElement($movie);
                        $entityManager->persist($genre_request);
                        $entityManager->remove($genre_request);
                    }
                }
            }

            foreach ($actors_repository as $actor_repository) {
                foreach ($movie->getActors() as $actor_request){
                    if($actor_request->getFullName() == $actor_repository->getFullName()){
                        $movie->getActors()->removeElement($actor_request);
                        $movie->addActor($actor_repository);
                        $entityManager->persist($movie);
                    }
                    if ($movie->getActors()->contains($actor_request) === false) {
                        $actor_request->getMovies()->removeElement($movie);
                        $entityManager->persist($actor_request);
                        $entityManager->remove($actor_request);
                    }
                }
            }

            /*Cover*/
            if(!is_null($movie->getCover())){ //   \(^^)/ !!!!!!!!!!!!!!!!!!!!
                $file = $movie->getCover();
                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();
                $movie->setCover($fileName);
                $file->move('files', $fileName);
            }

            $entityManager->persist($movie);
            $entityManager->flush();
            $edited = true;
            return $this->redirectToRoute('movie_edit', ['id' => $id]);
        }

        return $this->render('movies/edit.html.twig', [
            'form' => $form->createView(),
            'edited' => isset($edited) ? true : false
        ]);
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        return md5(uniqid());
    }
}
