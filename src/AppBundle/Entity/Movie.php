<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Movie
 */
class Movie
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $originalName;

    /**
     * @var string
     */
    private $dateFirst;

    /**
     * @var string
     */
    private $dateLast;

    /**
     * @var int
     */
    private $duration;

    /**
     * @var \DateTime
     */
    private $datePremiere;

    /**
     * @var string
     */
    private $preview;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     * @Assert\File(mimeTypes={ "image/jpeg" })
     */
    private $cover;

    /**
     * @var \AppBundle\Entity\Country
     */
    private $country;

    /**
     * @var \AppBundle\Entity\Producer
     */
    /*private $producer;*/

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $genres;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $actors;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->actors = new ArrayCollection();
        $this->genres = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Movie
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set originalName
     *
     * @param string $originalName
     *
     * @return Movie
     */
    public function setOriginalName($originalName)
    {
        $this->originalName = $originalName;

        return $this;
    }

    /**
     * Get originalName
     *
     * @return string
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }

    /**
     * Set dateFirst
     *
     * @param string $dateFirst
     *
     * @return Movie
     */
    public function setDateFirst($dateFirst)
    {
        $this->dateFirst = $dateFirst;

        return $this;
    }

    /**
     * Get dateFirst
     *
     * @return string
     */
    public function getDateFirst()
    {
        return $this->dateFirst;
    }

    /**
     * Set dateLast
     *
     * @param string $dateLast
     *
     * @return Movie
     */
    public function setDateLast($dateLast)
    {
        $this->dateLast = $dateLast;

        return $this;
    }

    /**
     * Get dateLast
     *
     * @return string
     */
    public function getDateLast()
    {
        return $this->dateLast;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     *
     * @return Movie
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set datePremiere
     *
     * @param \DateTime $datePremiere
     *
     * @return Movie
     */
    public function setDatePremiere($datePremiere)
    {
        $this->datePremiere = $datePremiere;

        return $this;
    }

    /**
     * Get datePremiere
     *
     * @return \DateTime
     */
    public function getDatePremiere()
    {
        return $this->datePremiere;
    }

    /**
     * Set preview
     *
     * @param string $preview
     *
     * @return Movie
     */
    public function setPreview($preview)
    {
        $this->preview = $preview;

        return $this;
    }

    /**
     * Get preview
     *
     * @return string
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Movie
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set country
     *
     * @param \AppBundle\Entity\Country $country
     *
     * @return Movie
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \AppBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Remove country
     *
     * @param \AppBundle\Entity\Country $actor
     */
    /*public function removeActor(Country $country)
    {
        $this->re($country);
    }*/

    /**
     * Set producer
     *
     * @param \AppBundle\Entity\Producer $producer
     *
     * @return Movie
     */
    /*public function setProducer(Producer $producer = null)
    {
        $this->producer = $producer;

        return $this;
    }*/

    /**
     * Get producer
     *
     * @return \AppBundle\Entity\Producer
     */
    /*public function getProducer()
    {
        return $this->producer;
    }*/

    /**
     * Add actor
     *
     * @param \AppBundle\Entity\Actor $actor
     *
     * @return Movie
     */
    public function addActor(Actor $actor)
    {

        if (!$this->actors->contains($actor)) {
            $this->actors->add($actor);
            $actor->addMovie($this);
        }
        return $this;
    }

    /**
     * Remove actor
     *
     * @param \AppBundle\Entity\Actor $actor
     */
    public function removeActor(Actor $actor)
    {
        $this->actors->removeElement($actor);
    }

    /**
     * Get actors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActors()
    {
        return $this->actors;
    }

    /**
     * Add genre
     *
     * @param \AppBundle\Entity\Genre $genre
     *
     * @return Movie
     */
    public function addGenre(Genre $genre)
    {
        if (!$this->genres->contains($genre)) {
            $this->genres->add($genre);
            $genre->addMovie($this);
        }
        return $this;
    }

    /**
     * Remove genre
     *
     * @param \AppBundle\Entity\Genre $genre
     */
    public function removeGenre(Genre $genre)
    {
        $this->genres->removeElement($genre);
    }

    /**
     * Get genres
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGenres()
    {
        return $this->genres;
    }

    /**
     * Set cover
     *
     * @param string $cover
     *
     * @return Movie
     */
    public function setCover($cover)
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * Get cover
     *
     * @return string
     */
    public function getCover()
    {
        return $this->cover;
    }
}
