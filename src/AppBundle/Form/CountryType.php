<?php

namespace AppBundle\Form;

use AppBundle\Entity\Country;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityManagerInterface;

class CountryType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $countries = $this->entityManager->getRepository(Country::class)->findAll();
        $builder->add('name', ChoiceType::class, [
            'choices'  => isset($countries)?$countries:[],
            'label' => false,
            'choice_label' => function($country, $key, $value) {
                return $country->getName();
            },
            'choice_value' => function (Country $country = null) {
                return $country ? $country->getName() : '';
            }
        ]);
        $builder->get('name')->resetViewTransformers();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Country::class,
        ]);
    }
}