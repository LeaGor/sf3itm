<?php

namespace AppBundle\Form;

use AppBundle\Entity\Genre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityManagerInterface;

class GenreType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $genres = $this->entityManager->getRepository(Genre::class)->findAll();
        $builder->add('name', ChoiceType::class, [
            'choices'  => isset($genres)?$genres:[],
            'label' => false,
            'choice_label' => function($genre, $key, $value) {
                return $genre->getName();
            },
            'choice_value' => function (Genre $genre = null) {
                return $genre ? $genre->getName() : '';
            }
        ]);
        $builder->get('name')->resetViewTransformers();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Genre::class,
        ]);
    }
}