<?php

namespace AppBundle\Form;

use AppBundle\Entity\Actor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityManagerInterface;

class ActorType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $actors = $this->entityManager->getRepository(Actor::class)->findAll();
        $builder->add('fullName', ChoiceType::class, [
            'choices'  => isset($actors)?$actors:[],
            'label' => false,
            'choice_label' => function($actor, $key, $value) {
                return $actor->getfullName();
            },
            'choice_value' => function (Actor $actor = null) {
                return $actor ? $actor->getFullName() : '';
            }
        ]);
        $builder->get('fullName')->resetViewTransformers();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Actor::class,
        ]);
    }
}