<?php

namespace AppBundle\Form;

use AppBundle\Entity\Movie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class MovieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Название *'
            ])
            ->add('originalName', TextType::class, [
                'label' => 'Оригинальное название',
                'required'   => false,
            ])
            ->add('dateFirst', IntegerType::class, [
                'label' => 'Год начала съемок',
                'attr' => ['min' => '1000', 'max' => '3000', 'placeholder' => '2000 г.', 'class' => 'year'],
                'required' => false
            ])
            ->add('dateLast', IntegerType::class, [
                'label' => 'Год окончания съемок',
                'attr' => ['min' => '1000', 'max' => '3000', 'placeholder' => '2000 г.', 'class' => 'year'],
                'required' => false
            ])
            ->add('datePremiere', DateType::class, [
                'label' => 'Дата премьеры',
                'widget' => 'single_text',
                'html5' => true,
                'required'   => false,
            ])
            ->add('duration', IntegerType::class, [
                'label' => 'Продолжительность (в секундах) *',
            ])
            ->add('preview', TextareaType::class, [
                'label' => 'Превью *'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Полное описание *'
            ])
            ->add('genres', CollectionType::class, [
                'entry_type' => GenreType::class,
                'label' => false,
                'allow_add' => true,
                'by_reference' => true,
                'allow_delete' => true,
            ])
            ->add('actors', CollectionType::class, [
                'entry_type' => ActorType::class,
                'label' => false,
                'allow_add' => true,
                'by_reference' => true,
                'allow_delete' => true,
            ])
            ->add('cover', FileType::class, [
                'label' => 'Обложка',
                'data_class' => null,
                'required'   => false,
            ])
            ->add('country', CountryType::class, [
                'label' => 'Страна',
                'required'   => false,
            ])
            /*
            ->add(
                'producer', CollectionType::class,
                [
                    'label' => 'Режиссер',
                ]
            )
            */
            ->add('save', SubmitType::class, [
                'label' => 'Сохранить'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Movie::class
        ));
    }
}
