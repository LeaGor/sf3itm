<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Actor;
use AppBundle\Entity\Country;
use AppBundle\Entity\Genre;
use AppBundle\Entity\Producer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Fixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // create
        // - 5 actors
        // - 5 countries
        // - 5 genres
        // - 5 producers
        for ($i = 0; $i < 5; $i++) {
            $actor = new Actor();
            $actor->setFullName('Actor_'.$i);
            $manager->persist($actor);

            $country = new Country();
            $country->setName('Country_'.$i);
            $manager->persist($country);

            $genre = new Genre();
            $genre->setName('Genre_'.$i);
            $manager->persist($genre);

            $producer = new Producer();
            $producer->setFullName('Producer_'.$i);
            $manager->persist($producer);
        }
        $manager->flush();
    }
}